import signal
import os

from datetime import datetime
from multiprocessing.connection import Listener
from random import gauss
from sys import exit
from time import sleep

import dotenv

dotenv.load_dotenv()

dt_format = os.getenv("DATETIME_FORMAT")

connection_auth_key = bytes(os.getenv("CONNECTION_AUTH_KEY"), "utf-8")
worker_address = os.getenv("WORKER_HOST")
worker_port = int(os.getenv("WORKER_PORT"))

listener = Listener((worker_address, worker_port), authkey=connection_auth_key)
conn = listener.accept()

process_wait_time = float(os.getenv("TIME_BETWEEN_REQUESTS"))
worker_sleep_time_range = (3,3) # seconds

def fmt_time() -> str:
    global dt_format

    return datetime.now().strftime(dt_format)

def sigint_handler(signum, frame):
    """Handler for SIGINT signals. This is the place to 'clean up'.
    """
    print(f"[worker] {fmt_time()}: SIGINT, exiting")
    
    conn.close()

    exit(0)

# Register shutdown signal ('Ctrl-C' <- `SIGINT`) handler
signal.signal(signal.SIGINT, sigint_handler)

job_no = 1
while True:
    sleep_time = round(abs(gauss(*worker_sleep_time_range)))

    if sleep_time < 1.0:
        sleep_time = 1.0

    print(f"[worker] {fmt_time()}: Start {job_no} => sleep for {sleep_time} seconds")

    conn.send(datetime.now())

    sleep(sleep_time)
    print(f"[worker] {fmt_time()}: Wait for {process_wait_time} second{'s' if process_wait_time > 1 else ''} to start next job")

    # Update timestamp so manager does not consider rest time as model run time
    conn.send(datetime.now())
    sleep(process_wait_time)

    job_no += 1