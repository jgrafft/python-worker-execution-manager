CREATE TABLE model_results (
    `dt` DateTime DEFAULT now(),
    `generation_start` DateTime NOT NULL,
    `generation_end` DateTime NOT NULL,
    `generation_elapsed_seconds` Float NOT NULL,
    `model` String NOT NULL,
    `device` String NOT NULL,
    `host` String NOT NULL,
    `prompt` String NOT NULL,
    `result` String NOT NULL,
) ENGINE = MergeTree ORDER BY `dt`;
