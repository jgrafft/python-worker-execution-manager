#############################################################################
# NOTE                                                                      #
#                                                                           #
# This is an example.                                                       #
#                                                                           #
# Sharing state using Environment Variables is a BAD IDEA, and VERY UNSAFE. #
#############################################################################

import asyncio

from datetime import datetime
from random import gauss

SHARED_ENVAR = "WORKER_LAST_PROC_START_TS"

dt_format = "%H:%M:%S.%f %Y-%m-%d"

global_timestamp = None
manager_wait_time_range = (6.3,2) # seconds
manager_sleep_time = 1/10 # 100 milliseconds
worker_sleep_time_range = (3,3) # seconds

def fmt_time():
    return datetime.now().strftime(dt_format)

async def manager() -> None:
    last_proc_start_seconds = datetime.now()
    manager_wait_time = round(abs(gauss(*manager_wait_time_range)), 3)
    number_of_restarts = 0
    queue = None
    worker_task = None

    if manager_wait_time < 5:
        manager_wait_time = 3.75

    print(f"[manager] {fmt_time()}: Manager wait time is {manager_wait_time} seconds")

    while True:
        if worker_task is None:
            print()
            print(f"[manager] {fmt_time()}: Starting worker (number_of_restarts = {number_of_restarts})")

            queue = asyncio.Queue(maxsize=1)
            worker_task = asyncio.ensure_future(worker(queue))

            number_of_restarts += 1
            last_proc_start_seconds = datetime.now()

        elif worker_task:
            # Maximum size of queue is one (1), so skip waiting if
            # there is no value available
            if queue.full():
                lmr_time_from_worker = await queue.get()

            if not worker_task.cancelled():
                last_proc_start_seconds = (datetime.now() - lmr_time_from_worker).total_seconds()

                if last_proc_start_seconds > manager_wait_time:
                    print(f"[manager] {fmt_time()}: Process has exceeded threshold of {manager_wait_time} seconds, cancel")

                    worker_task.cancel()
                    queue = None
                    worker_task = None
            else:
                worker_task = None

        await asyncio.sleep(manager_sleep_time)

async def worker(queue: asyncio.Queue) -> None:
    try:
        job_no = 1

        while True:
            # Draw a random time to sleep from a Gaussian distribution
            sleep_time = round(abs(gauss(*worker_sleep_time_range)), 3)

            if sleep_time == 0:
                sleep_time = 1

            print(f"[worker] {fmt_time()}: Start job {job_no} => sleep for {sleep_time} seconds")
            await queue.put(datetime.now())

            await asyncio.sleep(sleep_time)
            job_no += 1
    except asyncio.CancelledError:
        print(f"[worker] {fmt_time()}: Job number {job_no} canceled!")

if __name__ == "__main__":
    run_app = asyncio.ensure_future(manager())
    event_loop = asyncio.get_event_loop()
    event_loop.run_forever()