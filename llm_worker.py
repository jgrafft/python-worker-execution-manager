import os
import signal

from datetime import datetime

# TODO Refactor worker to be fully resilient to GPU model crashses
from multiprocessing.connection import Listener
from sys import exit
from time import sleep

import clickhouse_connect
import dotenv
import outlines

from example_model import Person

dotenv.load_dotenv()

### Ensure these are the same as 'manager.py' ###
dt_format = os.getenv("DATETIME_FORMAT")

# TODO Refactor worker to be fully resilient to GPU model crashses
connection_auth_key = bytes(os.getenv("CONNECTION_AUTH_KEY"), "utf-8")
worker_address = os.getenv("WORKER_HOST")
worker_port = int(os.getenv("WORKER_PORT"))

listener = Listener((worker_address, worker_port), authkey=connection_auth_key)
conn = listener.accept()

def fmt_time():
    global dt_format

    return datetime.now().strftime(dt_format)

def sigterm_handler(signum, frame):
    print(f"[worker] {fmt_time()}: SIGTERM, exiting")

    # TODO Refactor worker to be fully resilient to GPU model crashses
    if listener:
        conn.close()

    # Give time for connection to close
    sleep(3)
    exit(0)

print("Initialize session variables...")

# Persistence layer
client = clickhouse_connect.get_client(host=os.getenv("DB_hostname"), username=os.getenv("DB_USER"), password=os.getenv("DB_PASSWORD"), database=os.getenv("DB_DATABASE"))
_hostname = os.getenv("WORKER_HOSTNAME")
_table = os.getenv("DB_TABLE")
_sleep_time = int(os.getenv("TIME_BETWEEN_REQUESTS"))

_prompt = os.getenv("PROMPT")
_device = os.getenv("DEVICE")

_model = os.getenv("MODEL_NAME")
_model_kwargs = {}
_tokenizer_kwargs = {}

_requires_torch_dtype = [
     "h2oai/h2o-danube2-1.8b-sft",
     "meta-llama/Meta-Llama-3-8B",
     "microsoft/Phi-3-mini-128k-instruct",
     "microsoft/Phi-3-mini-4k-instruct"
] 

_requires_trust_remote_code = [
    "microsoft/Phi-3-mini-128k-instruct",
    "microsoft/Phi-3-mini-4k-instruct"
]

if _model in _requires_torch_dtype:
    import torch

    _model_kwargs["torch_dtype"] = torch.bfloat16

if _model in _requires_trust_remote_code:
    _model_kwargs["trust_remote_code"] = True

if os.getenv("USE_RANDOM_SEED").upper() == "TRUE":
    from random import randint
    import torch

    rng = torch.Generator(device=_device)
    rng.manual_seed(randint(0,31999))

print(f"Initiate '{_model}' on device '{_device}'...")
model = outlines.models.transformers(_model, device=_device, model_kwargs=_model_kwargs, tokenizer_kwargs=_tokenizer_kwargs)

print("Initiate generator...")
generator = outlines.generate.json(model, Person)

print("Generate models...")
print()

# Register shutdown handler
signal.signal(signal.SIGTERM, sigterm_handler)

job_no = 1
while True:
    t_start = datetime.now()
    print(f"{job_no}: {t_start.strftime('%Y-%m-%d %H:%M:%S')}")

    # Post start time for generation
    # TODO Refactor worker to be resilient to GPU model crashses
    conn.send(datetime.now())

    # NOTE Deterministic behavior is not desirable for this use case
    # result = generator(_prompt, rng=rng)

    result = generator(_prompt)

    t_end = datetime.now()
    t_elapsed = t_end - t_start

    client.insert(_table, [[t_start, t_end, t_elapsed.total_seconds(), _model, _device, _hostname, _prompt, result.json()]], column_names=["generation_start", "generation_end", "generation_elapsed_seconds", "model", "device", "host", "prompt", "result"])

    print(f"{job_no}: {t_end.strftime('%Y-%m-%d %H:%M:%S')}")
    print(f"{job_no}: {t_elapsed}")

    print()
    print(f"Sleep for {_sleep_time} second{'s' if _sleep_time > 1 else ''}")
    print()

    # Update timestamp so manager does not consider rest time as model run time
    # TODO Refactor worker to be resilient to GPU model crashses
    conn.send(datetime.now())

    sleep(_sleep_time)

    job_no += 1
