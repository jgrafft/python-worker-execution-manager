# LLM Worker Execution Manager

> Jason A. Grafft <<jason@grafft.co>>
>
> <https://gitlab.com/jgrafft/python-worker-execution-manager>

Scripts that demonstrate how to

1. Start a process with `subprocess`
1. Communicate between processes using `multiprocessing.connection`
1. Close the worker process with a `SIGINT` from the manager process
1. Close the manager and worker processes with a `SIGINT` (issued to the manager)

## `test_worker.py`

Minimum viable example of a worker script. Prints to `stdout`, sleeps, then prints to `stdout`. Communicates with the manager via `multiprocessing.connection`.

## `llm_worker.py`

Run harness for running LLM models using [outlines](https://github.com/outlines-dev/outlines). The example asks the chosen model to generate a Person from United States' Census demographic categories (encoded as a Pydantic model in `models.py`) then logs the result to ClickHouse.

## To Use

### Setup

1. Setup virtual environment and install dependencies
    - `cd worker-execution-manager-example`
    - `python -m venv .venv`
    - `source .venv/bin/activate`
    - `pip install -r requirements.txt`
1. Copy `.env.example` to `.env` and edit as necessary
    - If you add a model, you may also need to add it to the following lists in `llm_worker.py`
        - `_requires_torch_dtype`
        - `_requires_trust_remote_code`
    - Make sure to set `MANAGER_WAIT_TIME_MINUTES` to a value that makes sense for your model
        - You will likely need to test for your hardware

### Execution

- `cd worker-execution-manager-example`
- `source .venv/bin/activate`
- `python manager.py` (Use `Ctrl-C` to stop)

## Models

See System Specifications below for details on hardware.

Reminder that you will need to adjust the `MANAGER_WAIT_TIME_MINUTES` environment variable to an appropriate value for your model.

### gpt2-medium

| Device | Time per Model | Memory | Notes |
|--------|----------------|--------|-------|
| CPU    |                |        |       |
| GPU    | <10 seconds    |        |       |

### gpt2-xl

| Device | Time per Model | Memory | Notes |
|--------|----------------|--------|-------|
| CPU    |                |        |       |
| GPU    | ~30 seconds    |        |       |

### h2oai/h2o-danube2-1.8b-sft

| Device | Time per Model | Memory | Notes |
|--------|----------------|--------|-------|
| CPU    |                |        |       |
| GPU    | ~45 seconds    | ~4.8GB |       |

### meta-llama/Meta-Llama-3-8B


| Device | Time per Model   | Memory | Notes                                                 |
|--------|------------------|--------|-------------------------------------------------------|
| CPU    | ~[10,18] minutes |        |                                                       |
| GPU    |                  | ~7GB   | GPU will not accommodate, though model appears to fit |

### microsoft/phi-2

| Device | Time per Model | Memory | Notes                                                                                                 |
|--------|----------------|--------|-------------------------------------------------------------------------------------------------------|
| CPU    | ~100 seconds   | ~10GB  |                                                                                                       |
| GPU    | <30 seconds    |        | Run is flaky and crashes due to out-of-memory within [0,3] iterations. (No more buffer space on GPU?) |

### microsoft/Phi-3-mini-128k-instruct

| Device | Time per Model | Memory  | Notes                                              |
|--------|----------------|---------|----------------------------------------------------|
| CPU    |                | ~14.6GB |                                                    |
| GPU    |                | >12GB   | Cannot fit on current GPU (AMD Radeon RX 6700 XT). |

### microsoft/Phi-3-mini-4k-instruct

| Device | Time per Model | Memory  | Notes                                              |
|--------|----------------|---------|----------------------------------------------------|
| CPU    |                | ~14.3GB |                                                    |
| GPU    |                | >12GB   | Cannot fit on current GPU (AMD Radeon RX 6700 XT). |

### mistralai/Mistral-7B-Instruct-v0.2

| Device | Time per Model | Memory | Notes                                              |
|--------|----------------|--------|----------------------------------------------------|
| CPU    | ~5 minutes     | ~30GB  | Typically stalls after [1,3] generations.          |
| GPU    |                | >12GB  | Cannot fit on current GPU (AMD Radeon RX 6700 XT). |

## System Specifications

- **Processor:** AMD Ryzen 5 3400G @ 4.2 GHz
- **Memory:** 64 GB RAM
- **Graphics Card:** AMD Radeon RX 6700 XT 12 GB
- **Storage:** 1TB NVMe SSD
- **Operating System:** Ubuntu 22.04.4 LTS
- **Python Version:** Python 3.12.4

<!--CREATIVE COMMONS CC0 1.0 Universal-->
<hr />
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/jgrafft/python-worker-execution-manager">LLM Worker Execution Manager</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://grafft.co">Jason A. Grafft</a> is marked with <a href="https://creativecommons.org/publicdomain/zero/1.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC0 1.0 Universal<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/zero.svg?ref=chooser-v1" alt=""></a></p>
