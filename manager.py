import asyncio
import signal
import os
import subprocess

from datetime import datetime
from multiprocessing.connection import Client

import dotenv

dotenv.load_dotenv()

cmd = ["python", os.getenv("WORKER_SCRIPT")]

dt_format = os.getenv("DATETIME_FORMAT")

manager_sleep_time = float(os.getenv("MANAGER_SLEEP_TIME_SECONDS"))
manager_wait_time = float(os.getenv("MANAGER_WAIT_TIME_MINUTES")) * 60

# Interprocess communication settings
connection_auth_key = bytes(os.getenv("CONNECTION_AUTH_KEY"), "utf-8")
worker_address = os.getenv("WORKER_HOST")
worker_port = int(os.getenv("WORKER_PORT"))

def fmt_time() -> str:
    return datetime.now().strftime(dt_format)

async def manager() -> None:
    last_proc_start_seconds = 0
    number_of_restarts = 0
    worker_task = None

    try:
        while True:
            # Check if worker is active
            if worker_task is not None:
                worker_task_return_code = worker_task.returncode
            else:
                worker_task_return_code = -1

            # Worker return code is a value, so restart worker
            if worker_task_return_code is not None:
                print()
                print(f"[manager] {fmt_time()}: Starting worker (number_of_restarts = {number_of_restarts}, manager_wait_time = {manager_wait_time})")

                try:
                    # "Spawns" task on an independent thread
                    worker_task = subprocess.Popen(cmd, start_new_session=True)
                except Exception as ex:
                    print(f"EXCEPTION: {ex}")

                # Give time for worker to initialize
                await asyncio.sleep(5)

                # Establish connection with Listener
                ipc_client = Client(
                    (worker_address, worker_port),
                    authkey=connection_auth_key
                )

                number_of_restarts += 1
                last_proc_start_seconds = 0
                lmr_time_from_worker = datetime.now()
            else:
                # Worker return code is `None`, so worker is "alive and well"

                # Get communication from worker if there is one
                if ipc_client.poll():
                    lmr_time_from_worker = ipc_client.recv()

                # Calcuate time since last communication from worker
                last_proc_start_seconds = (datetime.now() - lmr_time_from_worker).total_seconds()

                # Stop process if 'manager_wait_time' threshold execeeded
                if last_proc_start_seconds > manager_wait_time:
                    print(f"[manager] {fmt_time()}: Process {worker_task.pid} has exceeded threshold of {manager_wait_time} seconds, cancel")

                    # Close Channel
                    ipc_client.close()

                    # Stop worker
                    worker_task.send_signal(signal.SIGINT)

                    worker_task = None
                    await asyncio.sleep(5)

            await asyncio.sleep(manager_sleep_time)
    except KeyboardInterrupt:
        print("[manager] KeyboardInterrupt, exiting")
        # Ensure process is stopped
        worker_task.send_signal(signal.SIGINT)

        await asyncio.sleep(1)
    except asyncio.CancelledError:
        # Handle cancellation signal
        print(f"[manager] {fmt_time()}: Process manager canceled")
        print(f"[manager] {fmt_time()}: Stopping process {worker_task.pid}")

        # Ensure process is stopped
        worker_task.send_signal(signal.SIGINT)

        await asyncio.sleep(1)

if __name__ == "__main__":
    asyncio.run(manager())